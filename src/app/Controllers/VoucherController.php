<?php
namespace App\Controllers;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\Voucher;
use App\Validator;

class VoucherController
{
    /**
     * Lists all vouchers
     * @param  Slim\Http\Request;  $request
     * @param  Slim\Http\Response; $response
     * @return Json
     */
    public function list(Request $request, Response $response)
    {
        $vouchers = Voucher::with('specialOffer')->get();
        return $response->withJson($vouchers);

    }

    /**
     * Generates Voucher Code for all users for given time and special offer
     * @param  Slim\Http\Request;  $request
     * @param  Slim\Http\Response; $response
     * @return Json
     */
    public function generateRecipientVoucher(Request $request, Response $response)
    {
        $expiration_date = $request->getParam('expiration_date');
        $special_offer_id = $request->getParam('special_offer_id');

        $validation = Validator::make($request, [
                    'expiration_date' => 'required|date_format:Y-m-d',
                    'special_offer_id' => 'required',
                ]);

        if ($validation->fails()) {
            return $response->withJson($validation->errors(), 406);
        }

        return $response->withJson(Voucher::generateRecipientVoucher($expiration_date, $special_offer_id));
    }

    /**
     * Generates Voucher Code for all users for given time and special offer
     * @param  Slim\Http\Request;  $request
     * @param  Slim\Http\Response; $response
     * @return Json
     */
    public function useVoucherCode(Request $request, Response $response)
    {
        $email = $request->getParam('email');
        $voucher_code = $request->getParam('voucher_code');

        $validation = Validator::make($request, [
                    'email' => 'required|email',
                    'voucher_code' => 'required',
                ]);

        if ($validation->fails()) {
            return $response->withJson($validation->errors(), 406);
        }
        $voucher = Voucher::useVoucherCode($email, $voucher_code);

        if ($voucher == 409) {
            return $response->withJson('Voucher Code Already Used', 409);
        }

        return $response->withJson('Percentage Discount rate : ' .  $voucher);
    }
}
