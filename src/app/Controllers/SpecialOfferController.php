<?php
namespace App\Controllers;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\SpecialOffer;
class SpecialOfferController
{
    /**
     * Lists all special offers
     * @param  Slim\Http\Request;  $request
     * @param  Slim\Http\Response; $response
     * @return Json
     */
    public function list(Request $request, Response $response)
    {
        $specialOffers = SpecialOffer::all();
        return $response->withJson($specialOffers);
    }
}
