<?php
namespace App\Controllers;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\Recipient;
use App\Validator;

class RecipientController
{
    /**
     * Lists all recipients
     * @param  Slim\Http\Request;  $request
     * @param  Slim\Http\Response; $response
     * @return Json
     */
    public function list(Request $request, Response $response)
    {
        $recipients = Recipient::with('vouchers')->get();
        return $response->withJson($recipients);

    }

    public function getRecipientVouchers(Request $request, Response $response)
    {
        $email = $request->getParam('email');
        $validation = Validator::make($request, [
                    'email' => 'required|email',
                ]);

        if ($validation->fails()) {
            return $response->withJson($validation->errors(), 406);
        }

        return $response->withJson(Recipient::getRecipientAvailableVouchers($email));

    }
}
