<?php

#Recipients Routes
$app->get('/recipients', 'App\Controllers\RecipientController:list')->setName('recipients');
$app->get('/recipient/get', 'App\Controllers\RecipientController:getRecipientVouchers');

#Special Offer Routes
$app->get('/special_offers', 'App\Controllers\SpecialOfferController:list')->setName('special_offers');


#Voucher Routes
$app->get('/vouchers', 'App\Controllers\VoucherController:list')->setName('vouchers');

$app->post('/generate_recipient_voucher', 'App\Controllers\VoucherController:generateRecipientVoucher')->setName('generate_recipient_voucher');

$app->post('/use_voucher_code', 'App\Controllers\VoucherController:useVoucherCode')->setName('use_voucher_code');
