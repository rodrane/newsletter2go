<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Recipient extends Model
{
    /**
     * Gets Voucher Codes of a Recipient
     */
    public function vouchers()
    {
        return $this->hasMany('App\Models\Voucher');
    }

    /**
     * Get Recipients Available Vouchers
     * @param  [string] $email
     * @return [Recipient]
     */
    public function getRecipientAvailableVouchers($email)
    {
        $recipients = Recipient::with(['vouchers' => function($q) {
            // Query the name field in status table
            $q->where('is_used', 0);
        }])
        ->where('email', $email)
        ->first();

        return $recipients;
    }
}
