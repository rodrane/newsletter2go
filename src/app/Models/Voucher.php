<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Recipient;
use App\Models\SpecialOffer;
use Carbon\Carbon;

class Voucher extends Model
{
    protected $fillable = ['voucher_code', 'expiration_date', 'special_offer_id', 'recipient_id', 'is_used', 'date_used'];
    public $timestamps = false;

    /**
     * Get Special Offer Code belongs to Voucher
     */
    public function specialOffer()
    {
        return $this->belongsTo('App\Models\SpecialOffer');
    }

    /**
     * Get Recipient belongs to Voucher
     */
    public function recipient()
    {
        return $this->belongsTo('App\Models\Recipient');
    }

    /**
     * Use Users Voucher Code
     * @param  [email] $voucher_code
     * @param  [string] $voucher_code
     * @return [bool]
     */
    public function useVoucherCode($email, $voucher_code)
    {
        $recipient = Recipient::where('email', $email)->first();
        $voucher = Voucher::with('specialOffer')
        ->where(['voucher_code' =>$voucher_code, 'recipient_id' => $recipient->id])->first();

        if (is_null ($voucher)) {
            throw new \App\Exceptions\VoucherException('Voucher Code Does Not Exist');
        }
        if ($voucher->is_used) {
            //Conflict Api Response
            return 409;
        }
        $voucher->update(['is_used' => 1, 'date_used' => Carbon::now()]);

        return $voucher->specialOffer->percentage_discount;

    }

    /**
     * Get Special Offer Code belongs to Voucher
     * @param  [datetime] $expiration_date
     * @param  [int] $special_offer_id
     * @return [bool]
     */
    public function generateRecipientVoucher($expiration_date, $special_offer_id)
    {
        $expiration_date = Carbon::createFromFormat('Y-m-d', $expiration_date);
        $now = Carbon::now();
        if (is_null (SpecialOffer::find($special_offer_id))) {
            throw new \App\Exceptions\SpecialOfferException('Special Offer Does not Exist');
        }

        if ( $now->diffInDays($expiration_date, false) < 0) {
            throw new \App\Exceptions\VoucherException('Expiration Date cannot be smaller than current day');
        }

        $recipients = Recipient::all();
        foreach ($recipients as $key=>$recipient) {
            $data[$key] = [
                'voucher_code' => uniqid(),
                'expiration_date' => $expiration_date,
                'special_offer_id' => $special_offer_id,
                'recipient_id' => $recipient->id
            ];
        }
        self::insert($data);

        return 200;
    }
}
