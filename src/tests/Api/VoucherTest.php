<?php

namespace Tests\Api;

use Tests\TestCase;
use Slim\Http\Uri;
use App\Models\Voucher;

class VoucherTest extends TestCase
{

    public function testIfVoucherssReturnsEndPointWork()
    {

        $response = $this->createRequestMock('GET', '/vouchers');
        $this->assertSame($response->getStatusCode(), 200);
    }

    public function testIfGenerateVoucherValidationWorks()
    {

        $response = $this->createRequestMock('POST', '/generate_recipient_voucher');

        $this->assertSame($response->getStatusCode(), 406);
    }

    public function testIfOldExpirationDateReturnsException()
    {
        $body = ['expiration_date' =>'1500-11-10', 'special_offer_id' => 1];

        $response = $this->createRequestMock('POST', '/generate_recipient_voucher', $body);
        $this->assertSame($response->getStatusCode(), 500);
    }

    public function testIfUnexistingOfferIdDateReturnsException()
    {
        $body = ['expiration_date' =>'2030-11-10', 'special_offer_id' => 000000];

        $response = $this->createRequestMock('POST', '/generate_recipient_voucher', $body);
        $this->assertSame($response->getStatusCode(), 500);
    }

    public function testIfCorrectBodyReturnsTrue()
    {
        $body = ['expiration_date' =>'2030-11-10', 'special_offer_id' => 1];

        #Requires Teardown() to with delete API to cleanup test data
        $response = $this->createRequestMock('POST', '/generate_recipient_voucher', $body);
        $this->assertSame($response->getStatusCode(), 200);
    }

    public function testIfUseVoucherCodeValidationWorks()
    {
        $body = [];

        $response = $this->createRequestMock('POST', '/use_voucher_code', $body);
        $this->assertSame($response->getStatusCode(), 406);
    }

    public function testIfAlreadyUsedVoucherCodesReturnsConflict()
    {
        #Requires Teardown() to with delete API to cleanup test data
        $body = ['voucher_code' => '5b751648c9100', 'email' =>'1@1.com' ];

        $response = $this->createRequestMock('POST', '/use_voucher_code', $body);

        $this->assertSame($response->getStatusCode(), 500);
    }

    public function testIfVoucherCodeUsedProperly()
    {
        $email = '1@1.com';

        #Requires Teardown() to with delete API to cleanup test data thats why requires manual search
        $unusedVoucherCode = Voucher::where(['is_used' => 0, 'recipient_id' => 1])->first();
        $body = ['voucher_code' => $unusedVoucherCode->voucher_code, 'email' =>$email];
        $response = $this->createRequestMock('POST', '/use_voucher_code', $body);

        $this->assertSame($response->getStatusCode(), 200);
    }
}
