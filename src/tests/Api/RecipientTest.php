<?php

namespace Tests\Api;

use Slim\Http\Environment;
use Slim\Http\Request;
use Tests\TestCase;

class RecipientTest extends TestCase
{

    public function testIfRecipientsReturnsEndPointWork()
    {
        $response = $this->createRequestMock('GET', '/recipients');
        $this->assertSame($response->getStatusCode(), 200);
    }

    public function testIfGetRecipientVouchersReturnsWork()
    {
        $response = $this->createRequestMock('GET', '/recipient/get?email=1@1.com');
        $this->assertSame($response->getStatusCode(), 200);
    }

    public function testIfEmailValidationWorks()
    {
        $response = $this->createRequestMock('GET', '/recipient/get');
        $this->assertSame($response->getStatusCode(), 406);
    }

}
