<?php

namespace Tests;

use PHPUnit\Framework\TestCase as BaseTestCase;
use Slim\Http\Environment;
use Slim\Http\Request;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function createRequestMock($method, $request_uri, $body = null)
    {
        $this->app = $this->createApplication();

        $env = Environment::mock([
            'REQUEST_METHOD' => $method,
            'REQUEST_URI'    => $request_uri,
        ]);

        $req = Request::createFromEnvironment($env)->withParsedBody($body);

        $this->app->getContainer()['request'] = $req;

        return $this->app->run(true);
    }
}
