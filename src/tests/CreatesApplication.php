<?php

namespace Tests;


trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        require __DIR__ . '/../vendor/autoload.php';


        // Instantiate the app
        $settings = require __DIR__ . '/../bootstrap/settings.php';
        $app = new \Slim\App($settings);

        //Set up Eloquent
        $container = $app->getContainer();
        $capsule = new \Illuminate\Database\Capsule\Manager;
        $capsule->addConnection($container->get('settings')['db']);
        $capsule->bootEloquent();


        // Set up dependencies
        require __DIR__ . '/../bootstrap/dependencies.php';

        // Register middleware
        require __DIR__ . '/../bootstrap/middleware.php';

        // Register routes
        require __DIR__ . '/../app/routes.php';
        return $app;

    }
}
