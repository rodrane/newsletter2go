# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.22)
# Database: testproject
# Generation Time: 2018-08-16 07:15:06 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table recipients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `recipients`;

CREATE TABLE `recipients` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `recipients` WRITE;
/*!40000 ALTER TABLE `recipients` DISABLE KEYS */;

INSERT INTO `recipients` (`id`, `email`, `name`)
VALUES
	(1,'1@1.com','1'),
	(2,'2@2.com','2'),
	(3,'3@3.com','3'),
	(4,'4@4.com','4');

/*!40000 ALTER TABLE `recipients` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table special_offers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `special_offers`;

CREATE TABLE `special_offers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `percentage_discount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `special_offers` WRITE;
/*!40000 ALTER TABLE `special_offers` DISABLE KEYS */;

INSERT INTO `special_offers` (`id`, `name`, `percentage_discount`)
VALUES
	(1,'Offer 1',50),
	(2,'Offer 2',25),
	(3,'Offer 3',10);

/*!40000 ALTER TABLE `special_offers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table vouchers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `vouchers`;

CREATE TABLE `vouchers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `voucher_code` varchar(13) NOT NULL DEFAULT '',
  `expiration_date` datetime NOT NULL,
  `is_used` tinyint(1) NOT NULL DEFAULT '0',
  `date_used` datetime DEFAULT NULL,
  `special_offer_id` int(11) DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `voucher_code` (`voucher_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `vouchers` WRITE;
/*!40000 ALTER TABLE `vouchers` DISABLE KEYS */;

INSERT INTO `vouchers` (`id`, `voucher_code`, `expiration_date`, `is_used`, `date_used`, `special_offer_id`, `recipient_id`)
VALUES
	(100,'5b750ef843cc4','2018-11-10 00:00:00',0,NULL,1,4),
	(101,'5b751188273f8','2018-11-10 00:00:00',0,NULL,1,1),
	(102,'5b75118827428','2018-11-10 00:00:00',0,NULL,1,2),
	(103,'5b75118827432','2018-11-10 00:00:00',0,NULL,1,3),
	(104,'5b7511882743b','2018-11-10 00:00:00',0,NULL,1,4),
	(105,'5b751648c90a5','2018-11-10 00:00:00',0,NULL,2,1),
	(106,'5b751648c9100','2018-11-10 00:00:00',1,'2018-08-16 07:09:22',2,2),
	(107,'5b751648c910b','2018-11-10 00:00:00',0,NULL,2,3),
	(108,'5b751648c9113','2018-11-10 00:00:00',0,NULL,2,4),
	(109,'5b75220b2fc9b','2018-11-10 00:00:00',0,NULL,1,1),
	(110,'5b75220b2fcb3','2018-11-10 00:00:00',0,NULL,1,2),
	(111,'5b75220b2fcbc','2018-11-10 00:00:00',0,NULL,1,3),
	(112,'5b75220b2fcc3','2018-11-10 00:00:00',0,NULL,1,4),
	(113,'5b75225800c2a','2018-11-10 00:00:00',0,NULL,1,1),
	(114,'5b75225800c49','2018-11-10 00:00:00',0,NULL,1,2),
	(115,'5b75225800c53','2018-11-10 00:00:00',0,NULL,1,3),
	(116,'5b75225800c5b','2018-11-10 00:00:00',0,NULL,1,4),
	(117,'5b752286d5119','2018-11-10 00:00:00',0,NULL,1,1),
	(118,'5b752286d5149','2018-11-10 00:00:00',0,NULL,1,2),
	(119,'5b752286d5155','2018-11-10 00:00:00',0,NULL,1,3),
	(120,'5b752286d515d','2018-11-10 00:00:00',0,NULL,1,4);

/*!40000 ALTER TABLE `vouchers` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
